import 'dart:convert';

import 'package:messenger/core/error/exceptions.dart';
import 'package:messenger/features/auth/data/datasources/auth_local_data_source.dart';
import 'package:messenger/features/auth/data/models/token_model.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

void main() {
  AuthLocalDataSourceImpl dataSource;
  MockSharedPreferences mockSharedPreferences;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    dataSource =
        AuthLocalDataSourceImpl(sharedPreferences: mockSharedPreferences);
  });

  group('getToken', () {
    final tTokenModel =
        TokenModel.fromJson(json.decode(fixture('token_cached.json')));

    test(
      'should return TokenModel from SharedPreferences when there is on in the cache',
      () async {
        // arrange
        when(mockSharedPreferences.getString(any))
            .thenReturn(fixture('token_cached.json'));
        // act
        final result = await dataSource.getToken();
        // assert
        verify(mockSharedPreferences.getString(ACCESS_TOKEN));
        expect(result, equals(tTokenModel));
      },
    );

    test(
      'should throw a CacheException when there is not a cached value',
      () async {
        // arrange
        when(mockSharedPreferences.getString(any)).thenReturn(null);
        // act
        final call = dataSource.getToken;
        // assert
        expect(() => call(), throwsA(isA<CacheException>()));
      },
    );
  });

  group('cacheToken', () {
    final tTokenModel = TokenModel(token: 'some_token');

    test(
      'should call SharedPreferences to cache the data',
      () async {
        // act
        dataSource.cacheToken(tTokenModel);
        // assert
        final expectedJsonString = json.encode(tTokenModel.toJson());
        verify(mockSharedPreferences.setString(
          ACCESS_TOKEN,
          expectedJsonString,
        ));
      },
    );
  });
}
