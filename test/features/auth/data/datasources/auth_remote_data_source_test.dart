import 'dart:convert';

import 'package:messenger/api/contracts/api_contract.dart';
import 'package:messenger/core/error/exceptions.dart';
import 'package:messenger/features/auth/data/datasources/auth_remote_data_source.dart';
import 'package:messenger/features/auth/data/models/token_model.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  AuthRemoteDataSourceImpl dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = AuthRemoteDataSourceImpl(client: mockHttpClient);
  });

  void setUpMockHttpClientGetSuccess() {
    when(mockHttpClient.get(
      any,
      headers: anyNamed('headers'),
    )).thenAnswer((_) async => http.Response('OK', 200));
  }

  void setUpMockHttpClientGetError({int responseCode}) {
    when(mockHttpClient.get(
      any,
      headers: anyNamed('headers'),
    )).thenAnswer((_) async => http.Response('Error', responseCode));
  }

  void setUpMockHttpClientPostSuccess({String responseFixture, int statusCode}) {
    when(mockHttpClient.post(
      any,
      body: anyNamed('body'),
      encoding: Encoding.getByName('utf-8'),
      headers: anyNamed('headers'),
    )).thenAnswer((_) async => http.Response(fixture(responseFixture), statusCode));
  }

  void setUpMockHttpClientPostError() {
    when(mockHttpClient.post(
      any,
      body: anyNamed('body'),
      encoding: Encoding.getByName('utf-8'),
      headers: anyNamed('headers'),
    )).thenAnswer((_) async => http.Response("Error", 400));
  }

  group('postRegistration', () {
    final tData = {'first_name': 'name'};

    test(
      'should perform a POST request on a URL with application/json header',
      () async {
        // arrange
        setUpMockHttpClientPostSuccess(
          responseFixture: 'responses/registration_response.json',
          statusCode: 201,
        );
        // act
        dataSource.postRegistration(tData);
        // assert
        verify(mockHttpClient.post(
          API_AUTH_REGISTER_URL,
          headers: {'Content-Type': 'application/json'},
          encoding: Encoding.getByName('utf-8'),
          body: json.encode(tData),
        ));
      },
    );

    test(
      'should return true when the response code is 201 (created)',
      () async {
        // arrange
        setUpMockHttpClientPostSuccess(
          responseFixture: 'responses/registration_response.json',
          statusCode: 201,
        );
        // act
        final result = await dataSource.postRegistration(tData);
        // assert
        expect(result, true);
      },
    );

    test(
      'should throw a ServerException when response code is not 201 (created)',
      () async {
        // arrange
        setUpMockHttpClientPostError();
        // act
        final call = dataSource.postRegistration;
        // assert
        expect(() => call(tData), throwsA(isA<ServerException>()));
      },
    );
  });

  group('postLogin', () {
    final tData = {'first_name': 'name'};
    final tToken = TokenModel.fromJson(json.decode(fixture('token.json')));

    test(
      'should perform a POST request on a URL with application/json header',
      () async {
        // arrange
        setUpMockHttpClientPostSuccess(
          responseFixture: 'token.json',
          statusCode: 200,
        );
        // act
        dataSource.postLogin(tData);
        // assert
        verify(mockHttpClient.post(
          API_AUTH_LOGIN_URL,
          headers: {'Content-Type': 'application/json'},
          encoding: Encoding.getByName('utf-8'),
          body: json.encode(tData),
        ));
      },
    );

    test(
      'should return TokenModel when the response code is 200 (ok)',
      () async {
        // arrange
        setUpMockHttpClientPostSuccess(
          responseFixture: 'token.json',
          statusCode: 200,
        );
        // act
        final result = await dataSource.postLogin(tData);
        // assert
        expect(result, tToken);
      },
    );

    test(
      'should throw a ServerException when response code is not 200 (ok)',
      () async {
        // arrange
        setUpMockHttpClientPostError();
        // act
        final call = dataSource.postLogin;
        // assert
        expect(() => call(tData), throwsA(isA<ServerException>()));
      },
    );
  });

  group('getCheckAuthentication', () {
    final tToken = TokenModel.fromJson(json.decode(fixture('token.json')));

    test(
      'should perform a GET request on a URL with application/json header',
          () async {
        // arrange
        setUpMockHttpClientGetSuccess();
        // act
        dataSource.getCheckAuthentication(tToken);
        // assert
        verify(mockHttpClient.get(
          API_AUTH_CHECK_URL,
          headers: {'Authorization': 'Bearer ${tToken.token}'},
        ));
      },
    );

    test(
      'should return true when the response code is 200 (ok)',
          () async {
        // arrange
        setUpMockHttpClientGetSuccess();
        // act
        final result = await dataSource.getCheckAuthentication(tToken);
        // assert
        expect(result, true);
      },
    );

    test(
      'should throw a ServerException when response code is not 200 (ok) or 401 (not authenticated)',
          () async {
        // arrange
        setUpMockHttpClientGetError(responseCode: 400);
        // act
        final call = dataSource.getCheckAuthentication;
        // assert
        expect(() => call(tToken), throwsA(isA<ServerException>()));
      },
    );

    test(
      'should throw a NotAuthenticatedException when response code is 401 (not authenticated)',
          () async {
        // arrange
        setUpMockHttpClientGetError(responseCode: 401);
        // act
        final call = dataSource.getCheckAuthentication;
        // assert
        expect(() => call(tToken), throwsA(isA<NotAuthenticatedException>()));
      },
    );
  });
}
