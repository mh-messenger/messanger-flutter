import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:messenger/features/auth/data/models/token_model.dart';
import 'package:messenger/features/auth/domain/entities/token.dart';

import '../../../../fixtures/fixture_reader.dart';

void main () {
  TokenModel tTokenModel;

  setUp(() {
    tTokenModel = TokenModel(token: 'some_token');
  });

  test(
    'should be a subclass of TokenModel',
    () async {
      // assert
      expect(tTokenModel, isA<Token>());
    },
  );
  
  group(
    'fromJson',
    () {
      test(
        'should return valid model',
        () async {
          // arrange
          final Map<String, dynamic> jsonMap = json.decode(fixture('token.json'));
          // act
          final result = TokenModel.fromJson(jsonMap);
          // assert
          expect(result, tTokenModel);
        },
      );
    }
  );

  group('toJson', () {
    test(
      'should return a valid JSON map containing the proper data',
      () {
        // act
        final result = tTokenModel.toJson();
        // assert
        final expectedMap = {
          'access_token': 'some_token'
        };
        expect(result, expectedMap);
      },
    );
  });
}