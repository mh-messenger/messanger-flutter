import 'package:dartz/dartz.dart';
import 'package:messenger/core/error/exceptions.dart';
import 'package:messenger/core/error/failures.dart';
import 'package:messenger/core/network/network_info.dart';
import 'package:messenger/features/auth/data/datasources/auth_local_data_source.dart';
import 'package:messenger/features/auth/data/datasources/auth_remote_data_source.dart';
import 'package:messenger/features/auth/data/models/token_model.dart';
import 'package:messenger/features/auth/data/repositories/auth_repository_impl.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockRemoteDataSource extends Mock implements AuthRemoteDataSource {}

class MockLocalDataSource extends Mock implements AuthLocalDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  AuthRepositoryImpl repository;
  MockRemoteDataSource mockRemoteDataSource;
  MockLocalDataSource mockLocalDataSource;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockRemoteDataSource = MockRemoteDataSource();
    mockLocalDataSource = MockLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = AuthRepositoryImpl(
      remoteDataSource: mockRemoteDataSource,
      localDataSource: mockLocalDataSource,
      networkInfo: mockNetworkInfo,
    );
  });

  void runTestsOnline(Function body) {
    group('device is online', ()
    {
      setUp(() {
        when(mockNetworkInfo.isConnected)
          ..thenAnswer((_) async => true);
      });

      body();
    });
  }

  void runTestsOffline(Function body) {
    group('device is offline', ()
    {
      setUp(() {
        when(mockNetworkInfo.isConnected)
          ..thenAnswer((_) async => false);
      });

      body();
    });
  }

  final tData = {'first_name': 'name'};

  final tTokenModel = TokenModel(token: 'some_token');

  group('postRegistration', () {
    test(
      'should check if the device online',
      () async {
        // arrange
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        // act
        repository.postRegistration(tData);
        // assert
        verify(mockNetworkInfo.isConnected);
      },
    );

    runTestsOnline(() {
      test(
        'should return remote data when the call to remote data source is successful',
            () async {
          // arrange
          when(mockRemoteDataSource.postRegistration(any))
              .thenAnswer((_) async => true);
          // act
          final result = await repository.postRegistration(tData);
          // assert
          verify(mockRemoteDataSource.postRegistration(tData));
          expect(result, Right(true));
        },
      );

      test(
        'should return server failure when the call to remote data source is unsuccessful',
            () async {
          // arrange
          when(mockRemoteDataSource.postRegistration(any))
              .thenThrow(ServerException());
          // act
          final result = await repository.postRegistration(tData);
          // assert
          verify(mockRemoteDataSource.postRegistration(tData));
          expect(result, Left(ServerFailure()));
        },
      );
    });

    runTestsOffline(() {
      test(
        'should return no network failure when the device is offline',
            () async {
          // arrange
          when(mockRemoteDataSource.postRegistration(any))
              .thenThrow(NoNetworkException());
          // act
          final result = await repository.postRegistration(tData);
          // assert
          verifyZeroInteractions(mockRemoteDataSource);
          expect(result, Left(NoNetworkFailure()));
        },
      );
    });
  });

  group('postLogin', () {
    test(
      'should check if the device online',
      () async {
        // arrange
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        // act
        repository.postLogin(tData);
        // assert
        verify(mockNetworkInfo.isConnected);
      },
    );

    runTestsOnline(() {
      test(
        'should return remote data when the call to remote data source is successful',
            () async {
          // arrange
          when(mockRemoteDataSource.postLogin(any))
              .thenAnswer((_) async => tTokenModel);
          // act
          final result = await repository.postLogin(tData);
          // assert
          verify(mockRemoteDataSource.postLogin(tData));
          expect(result, Right(tTokenModel));
        },
      );

      test(
        'should cache data when the call to remote data source is successful',
            () async {
          // arrange
          when(mockRemoteDataSource.postLogin(any))
              .thenAnswer((_) async => tTokenModel);
          // act
          await repository.postLogin(tData);
          // assert
          verify(mockRemoteDataSource.postLogin(tData));
          verify(mockLocalDataSource.cacheToken(tTokenModel));
        },
      );

      test(
        'should return server failure when the call to remote data source is unsuccessful',
            () async {
          // arrange
          when(mockRemoteDataSource.postLogin(any))
              .thenThrow(ServerException());
          // act
          final result = await repository.postLogin(tData);
          // assert
          verify(mockRemoteDataSource.postLogin(tData));
          expect(result, Left(ServerFailure()));
        },
      );
    });

    runTestsOffline(() {
      test(
        'should return no network failure when the device is offline',
            () async {
          // arrange
          when(mockRemoteDataSource.postLogin(any))
              .thenThrow(NoNetworkException());
          // act
          final result = await repository.postLogin(tData);
          // assert
          verifyZeroInteractions(mockRemoteDataSource);
          expect(result, Left(NoNetworkFailure()));
        },
      );
    });
  });

  group('getCheckAuthentication', () {
    test(
      'should check if the device online',
      () async {
        // arrange
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        // act
        repository.getCheckAuthentication();
        // assert
        verify(mockNetworkInfo.isConnected);
      },
    );

    runTestsOnline(() {
      test(
        'should return remote data when the call to remote data source is successful',
            () async {
          // arrange
          when(mockRemoteDataSource.getCheckAuthentication(tTokenModel))
              .thenAnswer((_) async => true);
          when(mockLocalDataSource.getToken())
              .thenAnswer((_) async => tTokenModel);
          // act
          final token = await mockLocalDataSource.getToken();
          final result = await repository.getCheckAuthentication();
          // assert
          verify(mockRemoteDataSource.getCheckAuthentication(token));
          expect(result, Right(true));
        },
      );

      test(
        'should throw NotAuthenticatedFailure when the call to remote data source is successful but user is not authenticated',
            () async {
          // arrange
          when(mockRemoteDataSource.getCheckAuthentication(any))
              .thenThrow(NotAuthenticatedException());
          // act
          final result = await repository.getCheckAuthentication();
          // assert
          expect(result, Left(NotAuthenticatedFailure()));
        },
      );

      test(
        'should throw CacheFailure when there is no token cached',
            () async {
          // arrange
          when(mockRemoteDataSource.getCheckAuthentication(any))
              .thenThrow(CacheException());
          // act
          final result = await repository.getCheckAuthentication();
          // assert
          expect(result, Left(CacheFailure()));
        },
      );

      test(
        'should throw ServerFailure when the call to remote data source is unsuccessful',
            () async {
          // arrange
          when(mockRemoteDataSource.getCheckAuthentication(any))
              .thenThrow(ServerException());
          // act
          final result = await repository.getCheckAuthentication();
          // assert
          expect(result, Left(ServerFailure()));
        },
      );
    });

    runTestsOffline(() {
      test(
        'should return no network failure when the device is offline',
            () async {
          // arrange
          when(mockRemoteDataSource.getCheckAuthentication(any))
              .thenThrow(NoNetworkException());
          // act
          final result = await repository.getCheckAuthentication();
          // assert
          verifyZeroInteractions(mockRemoteDataSource);
          expect(result, Left(NoNetworkFailure()));
        },
      );
    });
  });
}
