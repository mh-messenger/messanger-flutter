import 'package:dartz/dartz.dart';
import 'package:messenger/core/error/failures.dart';
import 'package:messenger/features/auth/data/models/token_model.dart';
import 'package:messenger/features/auth/presentation/bloc/auth_bloc.dart';
import 'package:messenger/features/auth/domain/usecases/get_check_authentication.dart';
import 'package:messenger/features/auth/domain/usecases/post_login.dart';
import 'package:messenger/features/auth/domain/usecases/post_registration.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockPostRegistration extends Mock implements PostRegistration {}

class MockPostLogin extends Mock implements PostLogin {}

class MockGetCheckAuthentication extends Mock
    implements GetCheckAuthentication {}

void main() {
  AuthBloc bloc;
  MockPostRegistration mockPostRegistration;
  MockPostLogin mockPostLogin;
  MockGetCheckAuthentication mockGetCheckAuthentication;

  setUp(() {
    mockPostRegistration = MockPostRegistration();
    mockPostLogin = MockPostLogin();
    mockGetCheckAuthentication = MockGetCheckAuthentication();
    bloc = AuthBloc(
      postRegistration: mockPostRegistration,
      postLogin: mockPostLogin,
      getCheckAuthentication: mockGetCheckAuthentication,
    );
  });

  tearDown(() {
    bloc?.close();
  });

  test('initialState should be AuthInitialState', () {
    expect(bloc.state, isA<AuthInitialState>());
  });

  group('PostRegistration', () {
    final tData = {'first_name': 'name'};

    test(
      'should post data to the useCase',
      () async {
        // arrange
        when(mockPostRegistration(any)).thenAnswer((_) async => Right(true));
        // act
        bloc.add(PostRegistrationEvent(tData));
        await untilCalled(mockPostRegistration(any));
        // assert
        verify(mockPostRegistration(tData));
      },
    );

    test(
      'should emit [PostRegistrationLoading, PostRegistrationDone] when data is gotten successfully',
      () async {
        // arrange
        when(mockPostRegistration(any)).thenAnswer((_) async => Right(true));
        // assert Later
        final expected = [
          PostRegistrationLoading(),
          PostRegistrationDone(message: 'Успешная регистрация'),
        ];
        expectLater(bloc, emitsInOrder(expected));

        // act
        bloc.add(PostRegistrationEvent(tData));
      },
    );

    test(
      'should emit [PostRegistrationLoading, PostRegistrationError] when data is gotten successfully',
          () async {
        // arrange
        when(mockPostRegistration(any)).thenAnswer((_) async => Left(ServerFailure()));
        // assert Later
        final expected = [
          PostRegistrationLoading(),
          AuthErrorState(message: "Регистрация не удалась!\nОшибка на сервере!"),
        ];
        expectLater(bloc, emitsInOrder(expected));

        // act
        bloc.add(PostRegistrationEvent(tData));
      },
    );

    test(
      'should emit [PostRegistrationLoading, PostRegistrationError] when data is gotten successfully',
          () async {
        // arrange
        when(mockPostRegistration(any)).thenAnswer((_) async => Left(NoNetworkFailure()));
        // assert Later
        final expected = [
          PostRegistrationLoading(),
          AuthErrorState(message: "Регистрация не удалась!\nОтсутствует подключение к интернету!"),
        ];
        expectLater(bloc, emitsInOrder(expected));

        // act
        bloc.add(PostRegistrationEvent(tData));
      },
    );
  });

  group('PostLogin', () {
    final tData = {'first_name': 'name'};
    final tToken = TokenModel(token: 'some_token');

    test(
      'should post data to the useCase',
          () async {
        // arrange
        when(mockPostLogin(any)).thenAnswer((_) async => Right(tToken));
        // act
        bloc.add(PostLoginEvent(tData));
        await untilCalled(mockPostLogin(any));
        // assert
        verify(mockPostLogin(tData));
      },
    );

    test(
      'should emit [PostLoginLoading, PostLoginDone] when data is gotten successfully',
          () async {
        // arrange
        when(mockPostLogin(any)).thenAnswer((_) async => Right(tToken));
        // assert Later
        final expected = [
          PostLoginLoading(),
          PostLoginDone(),
        ];
        expectLater(bloc, emitsInOrder(expected));

        // act
        bloc.add(PostLoginEvent(tData));
      },
    );

    test(
      'should emit [PostRegistrationLoading, PostRegistrationError] when data is gotten successfully',
          () async {
        // arrange
        when(mockPostLogin(any)).thenAnswer((_) async => Left(ServerFailure()));
        // assert Later
        final expected = [
          PostLoginLoading(),
          AuthErrorState(message: "Авторизация не удалась!\nОшибка на сервере!"),
        ];
        expectLater(bloc, emitsInOrder(expected));

        // act
        bloc.add(PostLoginEvent(tData));
      },
    );

    test(
      'should emit [PostRegistrationLoading, PostRegistrationError] when data is gotten successfully',
          () async {
        // arrange
        when(mockPostLogin(any)).thenAnswer((_) async => Left(NoNetworkFailure()));
        // assert Later
        final expected = [
          PostLoginLoading(),
          AuthErrorState(message: "Авторизация не удалась!\nОтсутствует подключение к интернету!"),
        ];
        expectLater(bloc, emitsInOrder(expected));

        // act
        bloc.add(PostLoginEvent(tData));
      },
    );
  });
}
