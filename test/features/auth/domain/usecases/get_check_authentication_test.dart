import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:messenger/features/auth/domain/repositories/auth_repository.dart';
import 'package:messenger/features/auth/domain/usecases/get_check_authentication.dart';
import 'package:mockito/mockito.dart';

class MockAuthRepository extends Mock implements AuthRepository {}

void main () {
  GetCheckAuthentication useCase;
  MockAuthRepository mockAuthRepository;

  setUp(() {
    mockAuthRepository = MockAuthRepository();
    useCase = GetCheckAuthentication(mockAuthRepository);
  });

  final tStatus = true;

  test(
    'should get true from the repository',
    () async {
      // arrange
      when(mockAuthRepository.getCheckAuthentication())
          .thenAnswer((_) async => Right(tStatus));
      // act
      final result = await useCase(NoParams());
      // assert
      expect(result, Right(tStatus));
      verify(mockAuthRepository.getCheckAuthentication());
      verifyNoMoreInteractions(mockAuthRepository);
    },
  );
}