import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:messenger/features/auth/domain/repositories/auth_repository.dart';
import 'package:messenger/features/auth/domain/usecases/post_registration.dart';
import 'package:mockito/mockito.dart';

class MockAuthRepository extends Mock implements AuthRepository {}

void main () {
  PostRegistration useCase;
  MockAuthRepository mockAuthRepository;

  setUp(() {
    mockAuthRepository = MockAuthRepository();
    useCase = PostRegistration(mockAuthRepository);
  });

  final tData = {
    'first_name': 'name'
  };
  final tStatus = true;

  test(
    'should put registration data to the repository',
    () async {
      // arrange
      when(mockAuthRepository.postRegistration(any))
          .thenAnswer((_) async => Right(tStatus));
      // act
      final result = await useCase(tData);
      // assert
      expect(result, Right(tStatus));
      verify(mockAuthRepository.postRegistration(tData));
      verifyNoMoreInteractions(mockAuthRepository);
    },
  );
}