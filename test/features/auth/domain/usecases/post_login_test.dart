import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:messenger/features/auth/domain/entities/token.dart';
import 'package:messenger/features/auth/domain/repositories/auth_repository.dart';
import 'package:messenger/features/auth/domain/usecases/post_login.dart';
import 'package:mockito/mockito.dart';

class MockAuthRepository extends Mock implements AuthRepository {}

void main () {
  PostLogin useCase;
  MockAuthRepository mockAuthRepository;

  setUp(() {
    mockAuthRepository = MockAuthRepository();
    useCase = PostLogin(mockAuthRepository);
  });

  final tData = {
    'email': 'example@example.com',
    'password': '12345678'
  };
  final Token tToken = Token('some_token');

  test(
    'should put login data to the repository',
    () async {
      // arrange
      when(mockAuthRepository.postLogin(any))
          .thenAnswer((_) async => Right(tToken));
      // act
      final result = await useCase(tData);
      // assert
      expect(result, Right(tToken));
      verify(mockAuthRepository.postLogin(tData));
      verifyNoMoreInteractions(mockAuthRepository);
    },
  );
}