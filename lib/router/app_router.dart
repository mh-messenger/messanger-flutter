import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:messenger/features/auth/presentation/pages/login_page.dart';
import 'package:messenger/features/auth/presentation/pages/register_page.dart';
import 'package:messenger/pages/contacts-page.dart';
import 'package:messenger/pages/home-page.dart';
import 'package:messenger/pages/splash-screen.dart';

class AppRouter {
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => SplashScreen());
      case '/home':
        return MaterialPageRoute(builder: (context) => HomePage());
      case '/contacts':
        return MaterialPageRoute(
          builder: (context) => ContactsPage(),
        );
      case '/auth/register':
        return MaterialPageRoute(
          builder: (context) => RegisterPage(),
        );
      case '/auth/login':
        return MaterialPageRoute(
          builder: (context) => LoginPage(),
        );
      default: return null;
    }
  }
}
