const String BASE_URL = 'http://mh-messenger-beckend.herokuapp.com/api';

/// Auth
const String API_AUTH_URL = '$BASE_URL/auth';

const String API_AUTH_REGISTER_URL = '$API_AUTH_URL/register';
const String API_AUTH_LOGIN_URL = '$API_AUTH_URL/login';
const String API_AUTH_CHECK_URL = '$API_AUTH_URL/check';