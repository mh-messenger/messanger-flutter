class NoNetworkException implements Exception {}
class ServerException implements Exception {}
class CacheException implements Exception {}
class NotAuthenticatedException implements Exception {}