import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  @override
  List<Object> get props => [];
}

// General failures

class NoNetworkFailure extends Failure {}
class ServerFailure extends Failure {}

class CacheFailure extends Failure {}
class NotAuthenticatedFailure extends Failure {}