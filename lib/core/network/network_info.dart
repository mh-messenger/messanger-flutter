import 'dart:io';

import 'package:data_connection_checker/data_connection_checker.dart';

abstract class NetworkInfo {
  Future<bool> get isConnected;
}

class NetworkInfoImpl implements NetworkInfo {
  final DataConnectionChecker connectionChecker;

  NetworkInfoImpl(this.connectionChecker) {
    this.connectionChecker.addresses = [
      AddressCheckOptions(InternetAddress('1.1.1.1')),
      AddressCheckOptions(InternetAddress('8.8.4.4')),
    ];
  }

  @override
  Future<bool> get isConnected => connectionChecker.hasConnection;

}