import 'package:flutter/material.dart';

class ChatPage extends StatefulWidget {
  final String name;

  ChatPage({this.name});

  @override
  _ChatPageState createState() => _ChatPageState(name: name);
}

class _ChatPageState extends State<ChatPage> {
  final name;

  _ChatPageState({
    this.name,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: FlatButton(
          child: Icon(Icons.chevron_left),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Row(
          children: [
            Icon(
              Icons.person,
              size: 40.0,
            ),
            SizedBox(
              width: 20.0,
            ),
            Text(this.name),
          ],
        ),
      ),
    );
  }
}
