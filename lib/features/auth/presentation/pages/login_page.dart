import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:messenger/features/auth/presentation/bloc/auth_bloc.dart';
import 'package:messenger/features/auth/presentation/widgets/login_form.dart';
import 'package:messenger/features/auth/presentation/widgets/auth_done.dart';
import 'package:messenger/features/auth/presentation/widgets/auth_fail.dart';
import 'package:messenger/injection_container.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return _buildPage(context);
  }

  Widget _buildPage(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Container(
          height: double.infinity,
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.10,
                child: Center(
                  child: Text(
                    'Авторизация',
                    style: Theme.of(context).textTheme.headline1,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.10),
                decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40.0),
                    topRight: Radius.circular(40.0),
                  ),
                ),
                child: BlocBuilder<AuthBloc, AuthState>(
                  builder: (BuildContext context, AuthState state) {
                    if (state is AuthInitialState) {
                      return Padding(
                        padding: EdgeInsets.all(20.0),
                        child: LoginForm(),
                      );
                    } else if (state is PostLoginLoading) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else if (state is PostLoginDone) {
                      return AuthDone(message: 'Усппешный вход!');
                    } else if (state is AuthErrorState) {
                      return AuthFail(message: state.message);
                    } else {
                      return Container();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
