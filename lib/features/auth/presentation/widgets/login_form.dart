import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:messenger/features/auth/presentation/bloc/auth_bloc.dart';
import 'package:messenger/widgets/primary_button.dart';
import 'package:messenger/widgets/text_area_with_label.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  String email;
  String password;

  Map<String, dynamic> loginData() {
    return {
      'email': email,
      'password': password,
    };
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextAreaWithLabel(
          label: 'E-mail',
          onChange: (value) => setState(() => this.email = value),
        ),
        TextAreaWithLabel(
          label: 'Пароль',
          onChange: (value) => setState(() => this.password = value),
        ),
        Spacer(),
        PrimaryButton(
          text: 'Войти',
          onTap: () {
            BlocProvider.of<AuthBloc>(context).add(PostLoginEvent(loginData()));
          },
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Нет аккаунта?',
                  style: Theme.of(context).textTheme.headline2),
              GestureDetector(
                onTap: () => Navigator.of(context).pushReplacementNamed('/auth/register'),
                child: Text(
                  'Зарегистрироваться',
                  style: Theme.of(context).textTheme.headline2.copyWith(
                        decoration: TextDecoration.combine(
                          [TextDecoration.underline],
                        ),
                      ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
