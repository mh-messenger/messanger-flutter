import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:messenger/features/auth/presentation/bloc/auth_bloc.dart';
import 'package:messenger/widgets/primary_button.dart';

class AuthFail extends StatelessWidget {
  final String message;

  const AuthFail({
    Key key,
    @required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Center(
            child: Text(
              this.message,
              style: Theme
                  .of(context)
                  .textTheme
                  .headline2,
            ),
          ),
        ),
        PrimaryButton(
          text: 'Попробовать снова',
          onTap: () {
            Navigator.of(context).pushNamed('/auth/login');
            BlocProvider.of<AuthBloc>(context).add(ClearStateEvent());
          },
        ),
      ],
    );
  }
}
