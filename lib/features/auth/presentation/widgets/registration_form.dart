import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:messenger/features/auth/presentation/bloc/auth_bloc.dart';
import 'package:messenger/widgets/primary_button.dart';
import 'package:messenger/widgets/text_area_with_label.dart';

class RegistrationForm extends StatefulWidget {
  @override
  _RegistrationFormState createState() => _RegistrationFormState();
}

class _RegistrationFormState extends State<RegistrationForm> {
  String firstName;
  String lastName;
  String middleName;
  String phone;
  String email;
  String password;

  Map<String, dynamic> registrationData() {
    return {
      'first_name': firstName,
      'last_name': lastName,
      'middle_name': middleName,
      'phone': phone,
      'email': email,
      'password': password,
    };
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextAreaWithLabel(
          label: 'Фамилия',
          onChange: (value) => setState(() => this.lastName = value),
        ),
        TextAreaWithLabel(
          label: 'Имя',
          onChange: (value) => setState(() => this.firstName = value),
        ),
        TextAreaWithLabel(
          label: 'Отчество',
          onChange: (value) => setState(() => this.middleName = value),
        ),
        TextAreaWithLabel(
          label: 'Телефон',
          onChange: (value) => setState(() => this.phone = value),
        ),
        TextAreaWithLabel(
          label: 'E-mail',
          onChange: (value) => setState(() => this.email = value),
        ),
        TextAreaWithLabel(
          label: 'Пароль',
          onChange: (value) => setState(() => this.password = value),
        ),
        PrimaryButton(
          text: 'Зарегистрироваться',
          onTap: () {
            BlocProvider.of<AuthBloc>(context)
                .add(PostRegistrationEvent(registrationData()));
          },
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Уже зарегистрировались?',
                style: Theme.of(context).textTheme.headline2,
              ),
              GestureDetector(
                onTap: () => Navigator.of(context).pushReplacementNamed('/auth/login'),
                child: Text(
                  'Войти',
                  style: Theme.of(context).textTheme.headline2.copyWith(
                        decoration: TextDecoration.combine(
                          [TextDecoration.underline],
                        ),
                      ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
