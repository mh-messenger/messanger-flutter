import 'package:flutter/material.dart';
import 'package:messenger/widgets/primary_button.dart';

class AuthDone extends StatelessWidget {
  final String message;

  const AuthDone({
    Key key,
    @required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Center(
            child: Text(
              this.message,
              style: Theme.of(context).textTheme.headline2,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.all(20.0),
          child: PrimaryButton(
            text: 'Начать общение',
            onTap: () => Navigator.of(context).pushReplacementNamed('/home'),
          ),
        ),
      ],
    );
  }
}
