part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class PostRegistrationEvent implements AuthEvent {
  final Map<String, dynamic> data;

  PostRegistrationEvent(this.data);

  @override
  List<Object> get props => [data];

  @override
  bool get stringify => false;
}

class PostLoginEvent extends AuthEvent {
  final Map<String, dynamic> data;

  PostLoginEvent(this.data);

  @override
  List<Object> get props => [data];

  @override
  bool get stringify => false;
}

class GetCheckAuthenticationEvent extends AuthEvent {

}

class ClearStateEvent extends AuthEvent {}