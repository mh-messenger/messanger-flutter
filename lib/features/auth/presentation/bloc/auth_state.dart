part of 'auth_bloc.dart';

@immutable
abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class AuthInitialState extends AuthState {}
class Authenticated extends AuthState {}

// Loading states
class PostRegistrationLoading extends AuthState {}
class PostLoginLoading extends AuthState {}

// Done states
class PostRegistrationDone extends AuthState {
  final String message;

  PostRegistrationDone({
    @required this.message,
  });

  @override
  List<Object> get props => [message];

  @override
  bool get stringify => false;

}
class PostLoginDone extends AuthState {}


// Error states
class AuthErrorState extends AuthState {
  final String message;

  AuthErrorState({
    @required this.message,
  });

  @override
  List<Object> get props => [message];

  @override
  bool get stringify => false;
}