import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:messenger/core/error/failures.dart';
import 'package:messenger/features/auth/domain/usecases/get_check_authentication.dart';
import 'package:messenger/features/auth/domain/usecases/post_login.dart';
import 'package:messenger/features/auth/domain/usecases/post_registration.dart';
import 'package:meta/meta.dart';

part 'auth_state.dart';
part 'auth_event.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final PostRegistration postRegistration;
  final PostLogin postLogin;
  final GetCheckAuthentication getCheckAuthentication;

  AuthBloc({
    @required this.postRegistration,
    @required this.postLogin,
    @required this.getCheckAuthentication,
  }) : super(AuthInitialState());

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    if (event is PostRegistrationEvent) {
      yield PostRegistrationLoading();
      final result = await postRegistration(event.data);

      yield* result.fold(
        (failure) async* {
          switch (failure.runtimeType) {
            case ServerFailure:
              yield AuthErrorState(
                  message: "Регистрация не удалась!\nОшибка на сервере!");
              break;
            case NoNetworkFailure:
              yield AuthErrorState(
                  message:
                      "Регистрация не удалась!\nОтсутствует подключение к интернету!");
              break;
            default:
              yield AuthErrorState(message: 'Неизвестная ошибка!');
          }
        },
        (answer) async* {
          this.add(PostLoginEvent({
            'email': event.data['email'],
            'password': event.data['password'],
          }));
          yield PostRegistrationDone(message: 'Успешная регистрация');
        },
      );
    } else if (event is PostLoginEvent) {
      yield PostLoginLoading();
      final result = await postLogin(event.data);

      yield* result.fold(
        (failure) async* {
          switch (failure.runtimeType) {
            case ServerFailure:
              yield AuthErrorState(
                  message: "Авторизация не удалась!\nОшибка на сервере!");
              break;
            case NoNetworkFailure:
              yield AuthErrorState(
                  message:
                  "Авторизация не удалась!\nОтсутствует подключение к интернету!");
              break;
            default:
              yield AuthErrorState(message: 'Неизвестная ошибка!');
          }
        },
        (_) async* {
          yield PostLoginDone();
        },
      );
    } else if (event is GetCheckAuthenticationEvent) {
      final result = await getCheckAuthentication(NoParams());

      yield* result.fold(
            (failure) async* {
          switch (failure.runtimeType) {
            case ServerFailure:
              yield AuthErrorState(
                  message: "Авторизация не удалась!\nОшибка на сервере!");
              break;
            case NoNetworkFailure:
              yield AuthErrorState(
                  message:
                  "Авторизация не удалась!\nОтсутствует подключение к интернету!");
              break;
            default:
              yield AuthErrorState(message: 'Неизвестная ошибка!');
          }
        },
            (_) async* {
          yield Authenticated();
        },
      );
    } else if (event is ClearStateEvent) {
      yield AuthInitialState();
    }
  }
}
