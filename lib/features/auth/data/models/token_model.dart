import 'package:meta/meta.dart';
import 'package:messenger/features/auth/domain/entities/token.dart';

class TokenModel extends Token {
  TokenModel({@required String token}) : super(token);

  factory TokenModel.fromJson(Map<String, dynamic> json) {
    return TokenModel(token: json['access_token']);
  }

  Map<String, dynamic> toJson() {
    return {
      'access_token': token
    };
  }
}