import 'package:meta/meta.dart';
import 'package:dartz/dartz.dart';
import 'package:messenger/core/error/failures.dart';
import 'package:messenger/core/error/exceptions.dart';
import 'package:messenger/core/network/network_info.dart';
import 'package:messenger/features/auth/data/datasources/auth_local_data_source.dart';
import 'package:messenger/features/auth/data/datasources/auth_remote_data_source.dart';
import 'package:messenger/features/auth/domain/entities/token.dart';
import 'package:messenger/features/auth/domain/repositories/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource remoteDataSource;
  final AuthLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  AuthRepositoryImpl({
    @required this.remoteDataSource,
    @required this.localDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, bool>> getCheckAuthentication() async {
    if(await networkInfo.isConnected) {
      try {
        final token = await localDataSource.getToken();
        final result = await remoteDataSource.getCheckAuthentication(token);
        return Right(result);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      } on NotAuthenticatedException {
        return Left(NotAuthenticatedFailure());
      }
    } else {
      return Left(NoNetworkFailure());
    }

  }

  @override
  Future<Either<Failure, Token>> postLogin(Map<String, dynamic> data) async {
    if (await networkInfo.isConnected) {
      try {
        final token = await remoteDataSource.postLogin(data);
        await localDataSource.cacheToken(token);
        return Right(token);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NoNetworkFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> postRegistration(Map<String, dynamic> data) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.postRegistration(data);
        return Right(result);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NoNetworkFailure());
    }
  }
}