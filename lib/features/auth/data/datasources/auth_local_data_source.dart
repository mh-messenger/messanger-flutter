import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:messenger/core/error/exceptions.dart';
import 'package:messenger/features/auth/data/models/token_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class AuthLocalDataSource {
  /// Gets the cached [TokenModel] which was saved after the last login
  ///
  /// Throws [CacheException] if no cached data is present
  Future<TokenModel> getToken();

  /// Saves the provided [TokenModel] in local cache
  Future<void> cacheToken(TokenModel tokenToCache);
}

const ACCESS_TOKEN = 'ACCESS_TOKEN';

class AuthLocalDataSourceImpl implements AuthLocalDataSource {
  final SharedPreferences sharedPreferences;

  AuthLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<void> cacheToken(TokenModel tokenToCache) {
    return sharedPreferences.setString(
      ACCESS_TOKEN,
      json.encode(
        tokenToCache.toJson(),
      ),
    );
  }

  @override
  Future<TokenModel> getToken() {
    final jsonString = sharedPreferences.getString(ACCESS_TOKEN);
    if (jsonString != null) {
      return Future.value(TokenModel.fromJson(json.decode(jsonString)));
    } else {
      throw CacheException();
    }
  }
}
