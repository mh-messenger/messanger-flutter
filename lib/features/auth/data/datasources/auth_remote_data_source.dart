import 'dart:convert';

import 'package:messenger/api/contracts/api_contract.dart';
import 'package:messenger/core/error/exceptions.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:messenger/features/auth/data/models/token_model.dart';
import 'package:messenger/features/auth/domain/entities/token.dart';

abstract class AuthRemoteDataSource {

  /// Calls the http://backend/api/auth/check endpoint
  ///
  /// Throws a [NotAuthenticatedException] if the status code is 401
  /// Throws a [ServerException] for another error codes
  Future<bool> getCheckAuthentication(TokenModel model);

  /// Calls the http://backend/api/auth/register endpoint
  ///
  /// Throws a [ServerException] for all error codes
  Future<Token> postLogin(Map<String, dynamic> data);

  /// Calls the http://backend/api/auth/login endpoint
  ///
  /// Throws a [ServerException] for all error codes
  Future<bool> postRegistration(Map<String, dynamic> data);
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  final http.Client client;

  AuthRemoteDataSourceImpl({
    @required this.client
  });

  @override
  Future<bool> getCheckAuthentication(TokenModel model) async {
    final response = await client.get(
      API_AUTH_CHECK_URL,
      headers: {
        'Authorization': 'Bearer ${model.token}',
      }
    );

    if(response.statusCode == 401) {
      throw NotAuthenticatedException();
    } else if (response.statusCode != 200) {
      throw ServerException();
    }

    return Future.value(true);
  }

  @override
  Future<Token> postLogin(Map<String, dynamic> data) async {
    final response = await client.post(
      API_AUTH_LOGIN_URL,
      body: json.encode(data),
      encoding: Encoding.getByName('utf-8'),
      headers: {
        'Content-Type': 'application/json'
      },
    );

    if(response.statusCode != 200) {
      throw ServerException();
    }

    return TokenModel.fromJson(json.decode(response.body));
  }

  @override
  Future<bool> postRegistration(Map<String, dynamic> data) async {
    final response = await client.post(
      API_AUTH_REGISTER_URL,
      body: json.encode(data),
      encoding: Encoding.getByName('utf-8'),
      headers: {
        'Content-Type': 'application/json'
      },
    );

    if(response.statusCode != 201) {
      throw ServerException();
    }

    return Future.value(true);
  }

}