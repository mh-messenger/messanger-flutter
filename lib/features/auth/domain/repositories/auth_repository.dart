import 'package:dartz/dartz.dart';
import 'package:messenger/core/error/failures.dart';
import 'package:messenger/features/auth/domain/entities/token.dart';

abstract class AuthRepository {
  Future<Either<Failure, bool>> getCheckAuthentication();
  Future<Either<Failure, Token>> postLogin(Map<String, dynamic> data);
  Future<Either<Failure, bool>> postRegistration(Map<String, dynamic> data);
}