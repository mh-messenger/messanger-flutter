import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:messenger/core/error/failures.dart';
import 'package:messenger/core/usecases/usecase.dart';
import 'package:messenger/features/auth/domain/repositories/auth_repository.dart';

class GetCheckAuthentication implements UseCase<bool, NoParams> {
  final AuthRepository repository;

  GetCheckAuthentication(this.repository);

  @override
  Future<Either<Failure, bool>> call(NoParams params) async {
    return await repository.getCheckAuthentication();
  }

}

class NoParams extends Equatable {
  const NoParams();

  @override
  List<Object> get props => [];

}