import 'package:dartz/dartz.dart';
import 'package:messenger/core/error/failures.dart';
import 'package:messenger/features/auth/domain/entities/token.dart';
import 'package:messenger/features/auth/domain/repositories/auth_repository.dart';
import 'package:messenger/core/usecases/usecase.dart';

class PostLogin implements UseCase<Token, Map<String, dynamic>> {
  final AuthRepository repository;

  PostLogin(this.repository);

  @override
  Future<Either<Failure, Token>> call(Map<String, dynamic> data) async {
    return await repository.postLogin(data);
  }

}