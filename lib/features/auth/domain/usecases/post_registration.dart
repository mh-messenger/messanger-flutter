import 'package:messenger/core/usecases/usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:messenger/core/error/failures.dart';
import 'package:messenger/features/auth/domain/repositories/auth_repository.dart';

class PostRegistration implements UseCase<bool, Map<String, dynamic>> {
  final AuthRepository repository;

  PostRegistration(this.repository);

  @override
  Future<Either<Failure, bool>> call (Map<String, dynamic> data) async {
    return await repository.postRegistration(data);
  }
}
