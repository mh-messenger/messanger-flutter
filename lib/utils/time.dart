import 'package:intl/intl.dart';

String buildTime(DateTime dateTime) {
  DateFormat dateFormat;
  String time;
  if (dateTime.day == DateTime.now().day) {
    dateFormat = new DateFormat.Hm();
    time = dateFormat.format(dateTime);
  } else if (dateTime.add(Duration(days: 1)).day == DateTime.now().day) {
    time = 'Вчера';
  } else {
    dateFormat = new DateFormat('dMy');
    time = dateFormat.format(dateTime);
  }

  return time;
}