import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:messenger/core/network/network_info.dart';
import 'package:messenger/features/auth/data/datasources/auth_local_data_source.dart';
import 'package:messenger/features/auth/data/datasources/auth_remote_data_source.dart';
import 'package:messenger/features/auth/data/repositories/auth_repository_impl.dart';
import 'package:messenger/features/auth/domain/repositories/auth_repository.dart';
import 'package:messenger/features/auth/domain/usecases/get_check_authentication.dart';
import 'package:messenger/features/auth/domain/usecases/post_login.dart';
import 'package:messenger/features/auth/domain/usecases/post_registration.dart';
import 'package:messenger/features/auth/presentation/bloc/auth_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init () async {
  ///----------------FEATURES----------------
  ///-------------Authentication-------------
  /// Bloc
  sl.registerFactory(() => AuthBloc(
    postRegistration: sl(),
    postLogin: sl(),
    getCheckAuthentication: sl(),
  ));

  /// UseCases
  sl.registerLazySingleton(() => PostRegistration(sl()));
  sl.registerLazySingleton(() => PostLogin(sl()));
  sl.registerLazySingleton(() => GetCheckAuthentication(sl()));

  /// Repository
  sl.registerLazySingleton<AuthRepository>(() => AuthRepositoryImpl(
    remoteDataSource: sl(),
    localDataSource: sl(),
    networkInfo: sl(),
  ));

  /// Data Sources
  sl.registerLazySingleton<AuthRemoteDataSource>(() => AuthRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<AuthLocalDataSource>(() => AuthLocalDataSourceImpl(sharedPreferences: sl()));

  ///------------------CORE------------------
  /// Network Info
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));


  ///----------------EXTERNAL----------------
  /// Shared Preferences
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton<SharedPreferences>(() => sharedPreferences);

  /// Http
  sl.registerLazySingleton(() => http.Client());

  /// Data Connection Checker
  sl.registerLazySingleton(() => DataConnectionChecker());
}