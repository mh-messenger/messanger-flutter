import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final String text;
  final Function onTap;

  const PrimaryButton({
    Key key,
    this.text,
    this.onTap
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      animationDuration: Duration(microseconds: 100),
      borderRadius: BorderRadius.all(
        Radius.circular(20.0),
      ),
      color: Theme.of(context).accentColor,
      child: InkWell(
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
        splashColor: Theme.of(context).primaryColor,
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(
              vertical: 10.0
          ),
          decoration: BoxDecoration(),
          child: Center(
            child: Text(
              this.text,
              style: Theme.of(context).textTheme.headline2,
            ),
          ),
        ),
        onTap: () {
          return this.onTap();
        },
      ),
    );
  }
}