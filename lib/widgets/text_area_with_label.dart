import 'package:flutter/material.dart';

class TextAreaWithLabel extends StatelessWidget {
  final String label;
  final Function onChange;

  const TextAreaWithLabel({Key key, this.label, this.onChange})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 10.0, left: 10.0, right: 10.0),
            child: Text(
              this.label,
              style: Theme.of(context).textTheme.headline2,
            ),
          ),
          TextField(
            style: Theme.of(context).textTheme.subtitle1,
            maxLines: 1,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Theme.of(context).accentColor),
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  )),
            ),
            onChanged: (value) {
              this.onChange(value);
            },
          ),
        ],
      ),
    );
  }
}
