import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:messenger/features/auth/domain/usecases/get_check_authentication.dart';
import 'package:messenger/features/auth/presentation/bloc/auth_bloc.dart';
import 'injection_container.dart' as sl;

import 'app.dart';

void main() async {
  _setupLogging();

  /// Register dependencies
  WidgetsFlutterBinding.ensureInitialized();
  await sl.init();

  /// Run application
  runApp(Application());
}

void _setupLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((rec) {
    print('[${rec.level.name}] |${rec.time}| ${rec.message}');
  });
}