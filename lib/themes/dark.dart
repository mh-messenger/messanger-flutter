import 'package:flutter/material.dart';

ThemeData basicTheme() {
  TextTheme _basicTextTheme(TextTheme base) {
    return base.copyWith(
      headline1: base.headline1.copyWith(
        fontFamily: 'Roboto',
        fontWeight: FontWeight.bold,
        fontSize: 24.0,
        color: Colors.white,
      ),
      headline2: base.headline2.copyWith(
        fontSize: 18.0,
        color: Colors.white,
      ),
      subtitle1: base.subtitle1.copyWith(
        fontSize: 14.0,
        fontWeight: FontWeight.w200,
        color: Colors.white,
      ),
    );
  }

  final ThemeData base = ThemeData.dark();
  return base.copyWith(
    scaffoldBackgroundColor: Color(0xff111122),
    textTheme: _basicTextTheme(base.textTheme),
    primaryColor: Color(0xff111122),
    accentColor: Color(0xff5555dd),
    buttonColor: Color(0xff333322),
    backgroundColor: Color(0xff202020),
  );
}
